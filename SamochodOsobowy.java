package Pojazd;

import java.util.TreeMap;

public class SamochodOsobowy extends PojazdMechaniczny {
	
	private int	liczbaPasażerów;
	private int	przewózPasażerów;
	
	public SamochodOsobowy() {
		super();
	}
	
	public SamochodOsobowy(String NumerRejestracyjny, TreeMap<String, Integer> Właściciel, int pojemnośćSilnika, String rodzajPaliwa, int liczbaPasażerów, int przewózPasażerów) {
		super(NumerRejestracyjny, Właściciel, pojemnośćSilnika, rodzajPaliwa);
		this.liczbaPasażerów = liczbaPasażerów;
		if (przewózPasażerów > liczbaPasażerów) {
			this.przewózPasażerów = liczbaPasażerów;
		} else {
			this.przewózPasażerów = przewózPasażerów;
		}
	}
	
	public String NumerRejestracyjny() {
		return super.NumerRejestracyjny();
	}
	
	public TreeMap<String, Integer> Właściciel() {
		return super.Właściciel();
	}
	
	public int pojemnośćSilnika() {
		return super.getPojemnośćSilnika();
	}
	
	public String rodzajPaliwa() {
		return super.getRodzajPaliwa();
	}
	
	public int getLiczbaPasażerów() {
		return liczbaPasażerów;
	}
	
	public void setLiczbaPasażerów(int liczbaPasażerów) {
		this.liczbaPasażerów = liczbaPasażerów;
	}
	
	public int getPrzewózPasażerów() {
		return przewózPasażerów;
	}
	
	public void setPrzewózPasażerów(int przewózPasażerów) {
		this.przewózPasażerów = przewózPasażerów;
	}
	
	@Override
	public String toString() {
		return "Osobówka [nr=" + NumerRejestracyjny() + ", Właściciel=" + Właściciel() + ", pojemność silnika=" + pojemnośćSilnika() + ", rodzaj paliwa=" + rodzajPaliwa()
				+ ", maksymalna liczba pasażerów="
				+ getLiczbaPasażerów() + ", aktualna liczba pasażerów=" + getPrzewózPasażerów() + ", czy ma Motor=" + isMotor() + "]";
	}
	
}
