package Pojazd;

import java.util.TreeMap;

public abstract class PojazdMechaniczny extends Pojazd {
	
	private int		pojemnośćSilnika;
	private String	rodzajPaliwa;
	
	public PojazdMechaniczny() {
		super();
		this.isMotor = true;
	}
	
	public PojazdMechaniczny(String NumerRejestracyjny, TreeMap<String, Integer> Właściciel, int pojemnośćSilnika, String rodzajPaliwa) {
		super(NumerRejestracyjny, Właściciel, true);
		this.pojemnośćSilnika = pojemnośćSilnika;
		this.rodzajPaliwa = rodzajPaliwa;
		
	}
	
	public String NumerRejestracyjny() {
		return super.getNumerRejestracyjny();
	}
	
	public boolean isMotor() {
		return super.isMotor();
	}
	
	public TreeMap<String, Integer> Właściciel() {
		return super.getWłaściciel();
	}
	
	public int getPojemnośćSilnika() {
		return pojemnośćSilnika;
	}
	
	public void setPojemnośćSilnika(int pojemnośćSilnika) {
		this.pojemnośćSilnika = pojemnośćSilnika;
	}
	
	public String getRodzajPaliwa() {
		return rodzajPaliwa;
	}
	
	public void setRodzajPaliwa(String rodzajPaliwa) {
		this.rodzajPaliwa = rodzajPaliwa;
	}
	
}
