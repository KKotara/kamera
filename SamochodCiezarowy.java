package Pojazd;

import java.util.TreeMap;

public class SamochodCiezarowy extends PojazdMechaniczny {
	
	private int	ładowność;
	private int	przewózŁadunku;
	
	public SamochodCiezarowy() {
		super();
	}
	
	public SamochodCiezarowy(String NumerRejestracyjny, TreeMap<String, Integer> Właściciel, int pojemnośćSilnika, String rodzajPaliwa, int ładowność, int przewózŁadunku) {
		super(NumerRejestracyjny, Właściciel, pojemnośćSilnika, rodzajPaliwa);
		this.ładowność = ładowność;
		if (przewózŁadunku > ładowność) {
			this.przewózŁadunku = ładowność;
		} else {
			this.przewózŁadunku = przewózŁadunku;
		}
	}
	
	public String NumerRejestracyjny() {
		return super.NumerRejestracyjny();
	}
	
	public TreeMap<String, Integer> Właściciel() {
		return super.Właściciel();
	}
	
	public int pojemnośćSilnika() {
		return super.getPojemnośćSilnika();
	}
	
	public String rodzajPaliwa() {
		return super.getRodzajPaliwa();
	}
	
	public int getŁadowność() {
		return ładowność;
	}
	
	public void setŁadowność(int ładowność) {
		this.ładowność = ładowność;
	}
	
	public int getPrzewózŁadunku() {
		return przewózŁadunku;
	}
	
	public void setPrzewózŁadunku(int przewózŁadunku) {
		this.przewózŁadunku = przewózŁadunku;
	}
	
	@Override
	public String toString() {
		return "Ciężarówka [nr=" + NumerRejestracyjny() + ", Właściciel=" + Właściciel() + ", pojemność silnika=" + pojemnośćSilnika() + ", rodzaj paliwa=" + rodzajPaliwa() + ", maksymalna ładowność="
				+ getŁadowność() + ", aktualna ładowność=" + getPrzewózŁadunku() + ", czy ma Motor=" + isMotor() + "]";
	}
	
}
