package Pojazd;

import java.util.TreeMap;

public class Rower extends Pojazd {
	
	private String	typy;
	private boolean	isMotor;
	
	public Rower() {
		super();
		this.isMotor = false;
	}
	
	public Rower(String NumerRejestracyjny, TreeMap<String, Integer> Właściciel, String typy) {
		super(NumerRejestracyjny, Właściciel, false);
		if (typowanie(typy)) {
			this.typy = typy;
		} else {
			this.typy = "turystyczny";
		}
	}
	
	public String NumerRejestracyjny() {
		return super.getNumerRejestracyjny();
	}
	
	public TreeMap<String, Integer> Właściciel() {
		return super.getWłaściciel();
	}
	
	public String getTypy() {
		return typy;
	}
	
	public void setTypy(String typy) {
		this.typy = typy;
	}
	
	public enum typ {
		turystyczny,
		górski,
		kolażówka
	}
	
	public boolean typowanie(String typTest) {
		for (typ Typ : typ.values()) {
			if (Typ.name()
					.equals(typTest)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Rower [nr=" + NumerRejestracyjny() + ", Właściciel=" + Właściciel() + ", type=" + typy + ", czy ma Motor=" + isMotor + "]";
	}
	
}
