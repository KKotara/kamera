package Pojazd;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeMap;

public class Main {
	
	static TreeMap<String, Integer>	pojazd1	= new TreeMap<String, Integer>();
	static TreeMap<String, Integer>	pojazd2	= new TreeMap<String, Integer>();
	static TreeMap<String, Integer>	pojazd3	= new TreeMap<String, Integer>();
	static TreeMap<String, Integer>	pojazd4	= new TreeMap<String, Integer>();
	static TreeMap<String, Integer>	pojazd5	= new TreeMap<String, Integer>();
	static TreeMap<String, Integer>	pojazd6	= new TreeMap<String, Integer>();
	
	static public ArrayList<Pojazd> tworzliste() {
		pojazd1.put("adam a", 55);
		pojazd1.put("ewa", 50);
		pojazd2.put("adam a", 55);
		pojazd3.put("ola", 50);
		pojazd3.put("adam", 55);
		pojazd3.put("ewa", 50);
		pojazd4.put("ja", 55);
		pojazd4.put("ty", 50);
		pojazd5.put("on", 55);
		pojazd6.put("ewa", 50);
		pojazd6.put("adam", 55);
		pojazd6.put("ja", 50);
		Rower nr1 = new Rower("01", pojazd1, "nic");
		Rower nr2 = new Rower("09", pojazd2, "kolażówka");
		SamochodCiezarowy nr3 = new SamochodCiezarowy("03", pojazd3, 500, "DAF", 5, 6);
		SamochodCiezarowy nr4 = new SamochodCiezarowy("04", pojazd4, 100, "BMW", 10, 8);
		SamochodOsobowy nr5 = new SamochodOsobowy("05", pojazd5, 50, "Audi", 5, 3);
		SamochodOsobowy nr6 = new SamochodOsobowy("07", pojazd6, 900, "BMW", 2, 3);
		System.out.println(nr1.toString());
		System.out.println(nr2.toString());
		System.out.println(nr3.toString());
		System.out.println(nr4.toString());
		System.out.println(nr5.toString());
		System.out.println(nr6.toString());
		
		ArrayList<Pojazd> pojazdy = new ArrayList<Pojazd>();
		pojazdy.add(nr1);
		pojazdy.add(nr2);
		pojazdy.add(nr3);
		pojazdy.add(nr4);
		pojazdy.add(nr5);
		pojazdy.add(nr6);
		return pojazdy;
	}
	
	static public HashSet<String> zad1(ArrayList<Pojazd> pojazdy) {
		HashSet<String> osoby = new HashSet<String>();
		for (Pojazd lokomocja : pojazdy) {
			osoby.addAll(lokomocja.getWłaściciel()
					.keySet());
		}
		return osoby;
	}
	
	static public int zad2(ArrayList<Pojazd> pojazdy) {
		int pojemność = 0;
		int liczba = 0;
		for (Pojazd lokomocja : pojazdy) {
			if (lokomocja.isMotor == true) {
				pojemność += ((PojazdMechaniczny) lokomocja).getPojemnośćSilnika();
				liczba++;
			}
		}
		return pojemność / liczba;
	}
	
	static public String zad3(String license, ArrayList<Pojazd> pojazdy) {
		for (Pojazd maszyna : pojazdy) {
			if (maszyna.isMotor()) {
				if (maszyna.getNumerRejestracyjny() == license) {
					return "Pojazd [nr=" + maszyna.getNumerRejestracyjny() + ", Właściciel=" + maszyna.getWłaściciel()
							.keySet() + ", pojemność silnika=" + ((PojazdMechaniczny) maszyna).getPojemnośćSilnika() + ", rodzaj paliwa=" + ((PojazdMechaniczny) maszyna).getRodzajPaliwa() + "] ";
				}
			}
		}
		return "Brak takiego pojazdu";
	}
	
	static public ArrayList<String> zad4(ArrayList<Pojazd> pojazdy) {
		ArrayList<String> osoby = new ArrayList<String>();
		Pojazd all = pojazdy.get(0);
		for (Pojazd lokomocja : pojazdy) {
			if (all.getWłaściciel()
					.size() == lokomocja.getWłaściciel()
							.size()) {
				osoby.add(lokomocja.getNumerRejestracyjny());
			} else if (all.getWłaściciel()
					.size() < lokomocja.getWłaściciel()
							.size()) {
				osoby.clear();
				osoby.add(lokomocja.getNumerRejestracyjny());
				all = lokomocja;
			}
		}
		return osoby;
	}
	
	static public HashSet<String> zad5(ArrayList<Pojazd> pojazdy) {
		HashSet<String> nosoby = new HashSet<String>();
		HashSet<String> tosoby = new HashSet<String>();
		for (Pojazd lokomocja : pojazdy) {
			if (lokomocja.isMotor == false) {
				nosoby.addAll(lokomocja.getWłaściciel()
						.keySet());
			}
			if (lokomocja.isMotor == true) {
				tosoby.addAll(lokomocja.getWłaściciel()
						.keySet());
			}
		}
		tosoby.retainAll(nosoby);
		return tosoby;
	}
	
	static public HashSet<String> zad6(ArrayList<Pojazd> pojazdy) {
		ArrayList<String> osoby = new ArrayList<String>();
		for (Pojazd lokomocja : pojazdy) {
			osoby.addAll(lokomocja.getWłaściciel()
					.keySet());
		}
		HashSet<String> powtarzającysię = new HashSet<String>();
		HashSet<String> unikalni = new HashSet<String>();
		
		for (String osoba : osoby) {
			if (!unikalni.add(osoba)) {
				powtarzającysię.add(osoba);
			}
		}
		return powtarzającysię;
	}
	
	public static void main(String[] args) {
		
		ArrayList<Pojazd> x = tworzliste();
		
		System.out.println(zad1(x));
		System.out.println(zad2(x));
		System.out.println(zad3("04", x));
		System.out.println(zad4(x));
		System.out.println(zad5(x));
		System.out.println(zad6(x));
	}
	
}
