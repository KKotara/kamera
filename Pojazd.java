package Pojazd;

import java.util.TreeMap;

public abstract class Pojazd {
	
	private TreeMap<String, Integer>	Właściciel;
	private String						NumerRejestracyjny;
	boolean								isMotor;
	
	public Pojazd() {
		
	}
	
	public Pojazd(String NumerRejestracyjny, TreeMap<String, Integer> Właściciel, boolean isMotor) {
		this.NumerRejestracyjny = NumerRejestracyjny;
		this.Właściciel = Właściciel;
		this.isMotor = isMotor;
	}
	
	public boolean maMotor() {
		if (this.isMotor) {
			return true;
		}
		return false;
	}
	
	public void setMotor(boolean isMotor) {
		this.isMotor = isMotor;
	}
	
	public boolean isMotor() {
		return isMotor;
	}
	
	public TreeMap<String, Integer> getWłaściciel() {
		return Właściciel;
	}
	
	public void setWłaściciel(TreeMap<String, Integer> właściciel) {
		Właściciel = właściciel;
	}
	
	public String getNumerRejestracyjny() {
		return NumerRejestracyjny;
	}
	
	public void setNumerRejestracyjny(String numerRejestracyjny) {
		NumerRejestracyjny = numerRejestracyjny;
	}
	
	public static void main(String[] args) {
		
	}
}